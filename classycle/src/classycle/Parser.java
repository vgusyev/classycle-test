/*
 * Copyright (c) 2003-2008, Franz-Josef Elmer, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package classycle;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.ooLab.language.javaByteCode.ClassFile;
import org.ooLab.language.javaByteCode.FieldInfo;
import org.ooLab.language.javaByteCode.InstructionHolder;
import org.ooLab.language.javaByteCode.JavaByteCodeException;
import org.ooLab.language.javaByteCode.MethodInfo;
import org.ooLab.language.javaByteCode.accessFlag.ClassAccessFlags;
import org.ooLab.language.javaByteCode.accessFlag.FieldAccessFlags;
import org.ooLab.language.javaByteCode.constantPool.ClassConstant;
import org.ooLab.language.javaByteCode.constantPool.FieldMethodInterfaceReferenceConstant;
import org.ooLab.language.javaByteCode.instruction.Instruction;
import org.ooLab.language.javaByteCode.instruction.constantPoolInstruction.fieldOrMethodInstruction.fieldInstruction.FieldInstruction;
import org.ooLab.language.javaByteCode.instruction.constantPoolInstruction.fieldOrMethodInstruction.fieldInstruction.GetField;
import org.ooLab.language.javaByteCode.instruction.constantPoolInstruction.fieldOrMethodInstruction.fieldInstruction.GetStaticField;
import org.ooLab.language.javaByteCode.instruction.constantPoolInstruction.fieldOrMethodInstruction.invokeInstruction.InvokeInstruction;

import classycle.classfile.StringConstant;
import classycle.classfile.UTF8Constant;
import classycle.graph.AtomicVertex;
import classycle.util.StringPattern;

/**
 * Utility methods for parsing class files and creating directed graphs. The
 * nodes of the graph are classes. The initial vertex of an edge is the class
 * which uses the class specified by the terminal vertex.
 * 
 * @author Franz-Josef Elmer
 */
public class Parser {
    public static final String ARCHIVE_PATH_DELIMITER = "::";
    private static final int ACC_INTERFACE = 0x200, ACC_ABSTRACT = 0x400;
    private static final String[] ZIP_FILE_TYPES = new String[] { ".zip",
            ".jar", ".war", ".ear" };
    private static final FileFilter ZIP_FILE_FILTER = new FileFilter() {
        public boolean accept(File file) {
            return isZipFile(file);
        }
    };

    /** Private constructor to prohibit instantiation. */
    private Parser() {
    }

    // /**
    // * Reads and parses class files and creates a direct graph. Short-cut of
    // * <tt>readClassFiles(classFiles, new {@link TrueStringPattern}(),
    // * null, false);</tt>
    // */
    // public static AtomicVertex[] readClassFiles(String[] classFiles)
    // throws IOException
    // {
    // return readClassFiles(classFiles, new TrueStringPattern(), null, false,
    // library);
    // }

    /**
     * Reads the specified class files and creates a directed graph where each
     * vertex represents a class. The head vertex of an arc is a class which is
     * used by the tail vertex of the arc. The elements of <tt>classFiles</tt>
     * are file names (relative to the working directory) which are interpreted
     * depending on its file type as
     * <ul>
     * <li>path of a class file (file type <tt>.class</tt>)
     * <li>path of a class file inside a ZIP file. The path has to contain both
     * paths: the path of the ZIP file first and the path of the class file in
     * the ZIP file second. Both have to be separated by '::'.
     * <li>path of a file of type <code>.zip</code>, <code>.jar</code>,
     * <code>.war</code>, or <code>.ear</code> containing class file
     * <li>path of a folder containing class files or zip/jar/war/ear files
     * </ul>
     * Folders and zip/jar/war/ear files are searched recursively for class
     * files. If a folder is specified only the top-level zip/jar/war/ear files
     * of that folder are analysed.
     * 
     * @param classFiles
     *            Array of file paths.
     * @param pattern
     *            Pattern fully qualified class names have to match in order to
     *            be added to the graph. Otherwise they count as 'external'.
     * @param reflectionPattern
     *            Pattern ordinary string constants of a class file have to
     *            fullfill in order to be handled as class references. In
     *            addition they have to be syntactically valid fully qualified
     *            class names. If <tt>null</tt> ordinary string constants will
     *            not be checked.
     * @param mergeInnerClasses
     *            If <code>true</code> merge inner classes with its outer class
     * @param library
     *            TODO
     * @return directed graph.
     */
    public static AtomicVertex[] readClassFiles(String[] classFiles,
            StringPattern pattern, StringPattern reflectionPattern,
            boolean mergeInnerClasses, boolean library) throws IOException {
        // ArrayList<UnresolvedNode> unresolvedNodes = new
        // ArrayList<UnresolvedNode>();
        Map<String, ZipFile> archives = new HashMap<String, ZipFile>();
        for (int i = 0; i < classFiles.length; i++) {
            String classFile = classFiles[i];
            int indexOfDelimiter = classFile.indexOf(ARCHIVE_PATH_DELIMITER);
            if (indexOfDelimiter >= 0) {
                String archivePath = classFile.substring(0, indexOfDelimiter);
                ZipFile zipFile = archives.get(archivePath);
                if (zipFile == null) {
                    zipFile = new ZipFile(archivePath);
                    archives.put(archivePath, zipFile);
                }
                classFile = classFile.substring(indexOfDelimiter
                        + ARCHIVE_PATH_DELIMITER.length());
                ZipEntry entry = zipFile.getEntry(classFile);
                analyseClassFileFromZipEntry(zipFile, entry, archivePath,
                        null/* unresolvedNodes */, reflectionPattern, library);
                continue;
            }
            File file = new File(classFile);
            if (file.isDirectory()) {
                analyseClassFile(file, classFile, null/* unresolvedNodes */,
                        reflectionPattern, library);
                File[] files = file.listFiles(ZIP_FILE_FILTER);
                for (int j = 0; j < files.length; j++) {
                    String source = createSourceName(classFile,
                            files[j].getName());
                    analyseClassFiles(new ZipFile(files[j].getAbsoluteFile()),
                            source, null/* unresolvedNodes */,
                            reflectionPattern, library);
                }
            } else if (file.getName().endsWith(".class")) {
                analyseClassFile(file, null, null/* unresolvedNodes */,
                        reflectionPattern, library);
            } else if (isZipFile(file)) {
                analyseClassFiles(new ZipFile(file.getAbsoluteFile()),
                        classFile, null/* unresolvedNodes */,
                        reflectionPattern, library);
            } else {
                throw new IOException(classFile + " is an invalid file.");
            }
        }
        // List<UnresolvedNode> filteredNodes = new ArrayList<UnresolvedNode>();
        // for (UnresolvedNode node : unresolvedNodes)
        // {
        // if (node.isMatchedBy(pattern))
        // {
        // filteredNodes.add(node);
        // }
        // }
        // UnresolvedNode[] nodes = new UnresolvedNode[filteredNodes.size()];
        // nodes = (UnresolvedNode[]) filteredNodes.toArray(nodes);
        // return GraphBuilder.createGraph(nodes, mergeInnerClasses);
        return null;
    }

    private static String createSourceName(String classFile, String name) {
        return classFile
                + (classFile.endsWith(File.separator) ? name
                        : File.separatorChar + name);
    }

    private static boolean isZipFile(File file) {
        boolean result = false;
        String name = file.getName().toLowerCase();
        for (int i = 0; i < ZIP_FILE_TYPES.length; i++) {
            if (name.endsWith(ZIP_FILE_TYPES[i])) {
                result = true;
                break;
            }
        }
        return result;
    }

    private static void analyseClassFile(File file, String source,
            ArrayList<UnresolvedNode> unresolvedNodes,
            StringPattern reflectionPattern, boolean library)
            throws IOException {
        if (file.isDirectory()) {
            String[] files = file.list();
            for (int i = 0; i < files.length; i++) {
                File child = new File(file, files[i]);
                if (child.isDirectory() || files[i].endsWith(".class")) {
                    analyseClassFile(child, source, unresolvedNodes,
                            reflectionPattern, library);
                }
            }
        } else {
            // unresolvedNodes.add(extractNode(file, source,
            // reflectionPattern));
            extractNode(file, source, reflectionPattern, library);
        }
    }

    private static UnresolvedNode extractNode(File file, String source,
            StringPattern reflectionPattern, boolean library)
            throws IOException {
        InputStream stream = null;
        UnresolvedNode result = null;
        try {
            stream = new FileInputStream(file);
            result = Parser.createNode(stream, source, (int) file.length(),
                    reflectionPattern, library);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                }
            }
        }
        return result;
    }

    private static void analyseClassFiles(ZipFile zipFile, String source,
            ArrayList<UnresolvedNode> unresolvedNodes,
            StringPattern reflectionPattern, boolean library)
            throws IOException {
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            analyseClassFileFromZipEntry(zipFile, entry, source,
                    unresolvedNodes, reflectionPattern, library);
        }
    }

    private static void analyseClassFileFromZipEntry(ZipFile zipFile,
            ZipEntry entry, String source,
            ArrayList<UnresolvedNode> unresolvedNodes,
            StringPattern reflectionPattern, boolean library)
            throws IOException {
        if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
            InputStream stream = zipFile.getInputStream(entry);
            int size = (int) entry.getSize();
//            unresolvedNodes.add(Parser.createNode(stream, source, size,
//                    reflectionPattern, library));
            Parser.createNode(stream, source, size,
                    reflectionPattern, library);
        }
    }

    /**
     * Creates a new node with unresolved references.
     * 
     * @param stream
     *            A just opended byte stream of a class file. If this method
     *            finishes succefully the internal pointer of the stream will
     *            point onto the superclass index.
     * @param source
     *            Optional source of the class file. Can be <code>null</code>.
     * @param size
     *            Number of bytes of the class file.
     * @param reflectionPattern
     *            Pattern used to check whether a {@link StringConstant} refer
     *            to a class. Can be <tt>null</tt>.
     * @param library
     *            TODO
     * @return a node with unresolved link of all classes used by the analysed
     *         class.
     */
    private static UnresolvedNode createNode(InputStream stream, String source,
            int size, StringPattern reflectionPattern, boolean library)
            throws IOException {
        // Reads constant pool, accessFlags, and class name
        // DataInputStream dataStream = new DataInputStream(stream);
        ClassFile cf = null;
        try {
            cf = new ClassFile(new DataInputStream(stream));
        } catch (JavaByteCodeException e) {
            cf = null;
        }
        // Constant[] pool = Constant.extractConstantPool(dataStream);
        ClassAccessFlags cafs = cf.getAccessFlags();
        // int accessFlags = dataStream.readUnsignedShort();
        String name = cf.getThisClass().getName().getText();

        ArrayList<String> interfaces = new ArrayList<String>();
        for (ClassConstant cc : cf.getInterfacesIndex()) {
            interfaces.add(cc.getName().getText());
        }

        JavaVertex classVertex;
        int innerClassPosition = name.lastIndexOf('$');
        if (innerClassPosition >= 0) {
            String outerClassName = name.substring(0, innerClassPosition);
            if (cafs.isInterface()) {
                classVertex = JavaVertex.declareInnerInterface(name,
                        outerClassName, interfaces, source, library);
            } else if (cafs.isEnum()) {
                classVertex = JavaVertex.declareInnerEnum(name, outerClassName,
                        source, library);
            } else {
                String superclass = cf.getSuperClass().getName().getText();
                classVertex = JavaVertex.declareInnerClass(name,
                        outerClassName, superclass, interfaces, source, library);
            }
        } else {
            if (cafs.isInterface()) {
                classVertex = JavaVertex.declareInterface(name, interfaces,
                        source, library);
                // } else if ( cafs.isAbstract() ) {
                // attributes = ClassAttributes.createAbstractClass(name,
                // source, size);
            } else if (cafs.isEnum()) {
                classVertex = JavaVertex.declareEnum(name, source, library);
            } else {
                String superclass = cf.getSuperClass().getName().getText();
                classVertex = JavaVertex.declareClass(name, superclass,
                        interfaces, source, library);
            }
        }

        // ((ClassConstant) pool[dataStream.readUnsignedShort()]).getName();
        // ClassAttributes attributes = null;
        // if( cafs.isInterface() ) {
        // attributes = ClassAttributes.createInterface(name, source, size);
        // } else if ( cafs.isAbstract() ) {
        // attributes = ClassAttributes.createAbstractClass(name, source, size);
        // } else {
        // attributes = ClassAttributes.createClass(name, source, size);
        // }

        // Creates a new node with unresolved references
        // UnresolvedNode node = new UnresolvedNode();
        // node.setAttributes(attributes);
        // ClassConstant superClass =
        // (ClassConstant)pool[dataStream.readUnsignedShort()];
        // node.addLinkTo(superClass.getName());
        // node.addLinkTo(cf.getSuperClass().getName().getText());
        // for( ClassConstant cc: cf.getInterfacesIndex() ) {
        // node.addLinkTo(cc.getName().getText());
        // }
        // for( int i = dataStream.readUnsignedShort(); i > 0; i-- ) { // skip
        // interfaces
        // ClassConstant interfaceClass =
        // (ClassConstant)pool[dataStream.readUnsignedShort()];
        // node.addLinkTo(interfaceClass.getName());
        // }
        for (FieldInfo fi : cf.getFields()) {
            addField(fi, classVertex, source, library);
        }
        // for( int i = dataStream.readUnsignedShort(); i > 0; i-- ) { // fields
        // addField(dataStream, node, pool);
        // }
        for (MethodInfo mi : cf.getMethods()) { // methods
            addMethod(mi, classVertex, source, library);
        }
        // for( int i = dataStream.readUnsignedShort(); i > 0; i-- ) { //
        // methods
        // addMethod(dataStream, node, pool);
        // }

        // for (int i = 1; i < pool.length; i++) {
        // Constant constant = pool[i];
        // // for( Constant constant: pool ) {
        // System.out.println("Pool constant " + constant.toString());
        // switch (constant.getType()) {
        // case Constant.CONSTANT_CLASS:
        // ClassConstant cc = (ClassConstant) constant;
        // if (!cc.getName().startsWith(("[")) && !cc.getName().equals(name)) {
        // node.addLinkTo(cc.getName());
        // }
        // break;
        // case Constant.CONSTANT_FIELDREF:
        // ///////////////////////////////////////////////////
        // // Search by methods where occurs
        // // Add symbolic link to the method
        // break;
        // case Constant.CONSTANT_METHODREF:
        //
        // /////////////////////////////////////////////////
        // break;
        // case Constant.CONSTANT_INTERFACE_METHODREF:
        // break;
        // case Constant.CONSTANT_STRING:
        // if (reflectionPattern != null) {
        // String str = ((StringConstant) constant).getString();
        // if (ClassNameExtractor.isValid(str) &&
        // reflectionPattern.matches(str)) {
        // node.addLinkTo(str);
        // }
        // }
        // break;
        // case Constant.CONSTANT_INTEGER:
        // break;
        // case Constant.CONSTANT_FLOAT:
        // break;
        // case Constant.CONSTANT_LONG:
        // break;
        // case Constant.CONSTANT_DOUBLE:
        // break;
        // case Constant.CONSTANT_NAME_AND_TYPE:
        // break;
        // case Constant.CONSTANT_UTF8:
        // parseUTF8Constant((UTF8Constant) constant, node, name);
        // break;
        // case Constant.CONSTANT_METHOD_HANDLE:
        // break;
        // case Constant.CONSTANT_METHOD_TYPE:
        // break;
        // case Constant.CONSTANT_INVOKE_DYNAMIC:
        // break;
        // }
        // if (constant instanceof ClassConstant) {
        // ClassConstant cc = (ClassConstant) constant;
        // if (!cc.getName().startsWith(("[")) && !cc.getName().equals(name))
        // {
        // node.addLinkTo(cc.getName());
        // }
        // } else if (constant instanceof UTF8Constant)
        // {
        // parseUTF8Constant((UTF8Constant) constant, node, name);
        // } else if (reflectionPattern != null
        // && constant instanceof StringConstant)
        // {
        // String str = ((StringConstant) constant).getString();
        // if (ClassNameExtractor.isValid(str) &&
        // reflectionPattern.matches(str))
        // {
        // node.addLinkTo(str);
        // }
        // }
        // }
        return null/* node */;
    }

    private static String extractTypeFromDescriptor(String descriptor) {
        int index = descriptor.lastIndexOf(')') + 1;
        String type = null;
        while (descriptor.charAt(index) == '[') {
            index++;
        }
        if (descriptor.charAt(index) == 'L') {
            type = descriptor.substring(index + 1, descriptor.length() - 1);
        }
        return type;
    }

    public static int extractTypeNameFromDescriptor(StringBuffer result,
            String descriptor, int start) {
        int arrayCount = 0;
        while (descriptor.charAt(start) == '[') {
            arrayCount++;
            start++;
        }
        String typeName = "unknown";
        int nextStart = start;
        switch (descriptor.charAt(start)) {
        case 'B':
            typeName = "byte";
            break;
        case 'C':
            typeName = "char";
            break;
        case 'D':
            typeName = "doouble";
            break;
        case 'F':
            typeName = "float";
            break;
        case 'I':
            typeName = "int";
            break;
        case 'J':
            typeName = "long";
            break;
        case 'S':
            typeName = "short";
            break;
        case 'Z':
            typeName = "boolean";
            break;
        case 'L':
            nextStart = descriptor.indexOf(';', start + 1);
            typeName = descriptor.substring(start + 1, nextStart);
            JavaVertex.getVertex(typeName);
            break;
        }
        result.append(typeName);
        for (int i = 0; i < arrayCount; i++) {
            result.append("[]");
        }
        return nextStart + 1;
    }

    private static String extractParamsFromDescriptor(String descriptor) {
        StringBuffer result = new StringBuffer("(");
        int index = 1;
        while (descriptor.charAt(index) != ')') {
            if (result.length() > 1) {
                result.append(',');
            }
            index = extractTypeNameFromDescriptor(result, descriptor, index);
        }
        result.append(')');
        return result.toString();
    }

    private static void addField(FieldInfo fi, JavaVertex node, String source, boolean library) {
        JavaVertex field = node.declareField(fi.getName().getText(), source, library);
        String fieldtype = extractTypeFromDescriptor(fi.getDescriptor().getText());
        field.addReference(fieldtype);
        FieldAccessFlags fieldflags = fi.getAccessFlags(); 
        if (fieldflags.isSynthetic()
                || fieldflags.isStatic()
                && fieldflags.isFinal()
                && (fieldflags.isEnum() 
                || fieldtype == null
                || fieldtype.equals("java/lang/String"))) {
            field.setCohesiveForced();
        }
        if( fieldflags.isStatic() ) {
            field.setStatic();
        }
    }

    private static void addMethod(MethodInfo mi, JavaVertex node,
            String source, boolean library) {
        String name = mi.getName().getText();
        JavaVertex method;
        String methodName;
        String params = extractParamsFromDescriptor(mi.getDescriptor()
                .getText());
        methodName = name + params;
        if (name.equals("<init>")) {
            method = node.declareConstructor(methodName, source, library);
        } else if (name.equals("<clinit>")) {
            method = node.declareConstructor(methodName, source, library);
            node.addCalling(method.name);
        } else {
            method = node.declareMethod(methodName, source, library);
        }
        if( library && mi.getAccessFlags().isAbstract() ) {
            node.addCalling(method.name);
        }
        method.addReference(extractTypeFromDescriptor(mi.getDescriptor()
                .getText()));
        if (mi.getAccessFlags().isSynthetic()
                || node.type == JavaVertex.Type.ENUMERATION
                && (method.type == JavaVertex.Type.CONSTRUCTOR
                        && params.equals("(java/lang/String,int)")
                        || methodName.equals("values()") || methodName
                            .equals("valueOf(java/lang/String)"))) {
            method.setCohesiveForced();
        }
        if( mi.getAccessFlags().isStatic() ) {
            method.setStatic();
        }
        if (!mi.getAccessFlags().isAbstract()
                && !mi.getAccessFlags().isNative()) {
            for (Iterator<InstructionHolder> it = mi.getCodeAttribute()
                    .getInstructions().iterator(); it.hasNext();) {
                Instruction ih = it.next().getInstruction();
                if (ih instanceof GetField
                    || ih instanceof GetStaticField ) {
                    FieldMethodInterfaceReferenceConstant arg = (FieldMethodInterfaceReferenceConstant) ((FieldInstruction) ih)
                            .getConstantEntry();
                    String classname = arg.getClassConstant().getName().getText();
                    JavaVertex.getVertex(classname);
                    String link = classname
                            + '.'
                            + arg.getNameAndTypeConstant().getName().getText();
                    method.addReference(link);
                } else if (ih instanceof InvokeInstruction) {
                    FieldMethodInterfaceReferenceConstant arg = (FieldMethodInterfaceReferenceConstant) ((InvokeInstruction) ih)
                            .getConstantEntry();
                    String classname = arg.getClassConstant().getName().getText();
                    JavaVertex.getVertex(classname);
                    String link = classname
                            + '.'
                            + arg.getNameAndTypeConstant().getName().getText()
                            + extractParamsFromDescriptor(arg
                                    .getNameAndTypeConstant().getDescriptor()
                                    .getText());
                    method.addCalling(link);
                }
            }
        }
        // node.addMethod(mi.getName().getText(), mi.getDescriptor().getText(),
        // nodes);
    }

    /**
     * Parses an UFT8Constant and picks class names if it has the correct syntax
     * of a field or method descirptor.
     */
    static void parseUTF8Constant(UTF8Constant constant, UnresolvedNode node,
            String className) {
        Set<String> classNames = new ClassNameExtractor(constant).extract();
        for (String element : classNames) {
            if (className.equals(element) == false) {
                node.addLinkTo(element);
            }
        }
    }

} // class