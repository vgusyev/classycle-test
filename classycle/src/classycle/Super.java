package classycle;

public class Super {
    int i;
    Sub a;
    void m() {
    }
    void a() {
        a = new Sub();
        a.m();
    }
    void x(Super p) {
        p.m();
    }
    static void MAIN() {
    }
}
