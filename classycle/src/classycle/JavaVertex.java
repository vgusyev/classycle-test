package classycle;

import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

public class JavaVertex {

    private static TreeMap<String, JavaVertex> GRAPH = new TreeMap<String, JavaVertex>();

    public static void report(OutputStream stream, List<String> entryNames) {
        PrintStream out = new PrintStream(stream);

        List<String> foundEntries = new ArrayList<String>();
        out.println("Entry points:");
        for (String entryName : entryNames) {
            out.print("\t" + entryName);
            JavaVertex vertex = GRAPH.get(entryName);
            if (vertex == null) {
                out.print("... not found!");
            } else {
                foundEntries.add(entryName);
            }
            out.println();
        }
        if (foundEntries.size() == 0) {
            System.err.println("No entry points found. Exiting...");
        } else {
            List<JavaVertex> list = reportIsolated(foundEntries);
            if (list.isEmpty()) {
                out.println("None of elements can be proposed for removal");
            } else {
                out.println("Elements collected:");
                for (Iterator<JavaVertex> it = GRAPH.values().iterator(); it
                        .hasNext();) {
                    JavaVertex vertex = it.next();
                    out.println(String.format("\t%02x:%s", vertex.cohesion,
                            vertex));
                    for (Iterator<JavaVertex.Arc> jt = vertex.arcs.iterator(); jt
                            .hasNext();) {
                        JavaVertex.Arc arc = jt.next();
                        out.println(String.format("\t\t%s\t%02x:%s",
                                arc.kind.name(), arc.vertex.cohesion,
                                arc.vertex));
                    }
                }

                out.println("Elements proposed for removal (check methods for overriding before removing):");
                for (JavaVertex vertex : list) {
                    out.println("\t" + vertex);
                }
            }
        }
        out.flush();
    }

    public static List<JavaVertex> reportIsolated(List<String> entryNames) {
        List<JavaVertex> entries = new ArrayList<JavaVertex>();
        List<JavaVertex> isolated = new ArrayList<JavaVertex>();

        for (String entry : entryNames) {
            entries.add(getVertex(entry));
        }
        // for( Iterator<JavaVertex> it = GRAPH.values().iterator();
        // it.hasNext(); ) {
        // JavaVertex vertex = it.next();
        // vertex.reset();
        // }
        resolveUnknownTypesByReflection();
        InvocationBinder.bindMostSpecificMethods(GRAPH);
        bindOverridingMethods();
        lookup(entries, COHESION_PLAIN);
        lookup(entries, COHESION_RESULTANT);
        for (Iterator<JavaVertex> it = GRAPH.values().iterator(); it.hasNext();) {
            JavaVertex vertex = it.next();
            JavaVertex container = vertex.container();
            if (container == null || container.cohesive(COHESION_RESULTANT)) {
                if (vertex.type != Type.UNRESOLVED
                        && !vertex.cohesive(COHESION_LIBRARY | COHESION_FORCED
                                | COHESION_RESULTANT)) {
                    isolated.add(vertex);
                }
            }
        }
        return isolated;
    }

    private void topVertex(String source, boolean library) {
        if( library ) {
            boolean synthetic = source == null;
            addArc(Arc.Kind.Container,
                getVertex(Type.LIBRARY, synthetic ? "<unknown>" : source, source, synthetic));
        } else {
            addArc(Arc.Kind.Container,
                getVertex(Type.PACKAGE, name.substring(0, name.lastIndexOf('/')), source, false));
        }
    }
    
    public static JavaVertex declareClass(String name, String superclass,
            List<String> interfaces, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.CLASS, name, source, library);
        vertex.topVertex(source, library);
        vertex.addInheriting(superclass);
        for (String interfaceName : interfaces) {
            vertex.addInheriting(interfaceName);
        }
        return vertex;
    }

    public static JavaVertex declareInnerClass(String name, String className,
            String superclass, List<String> interfaces, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.CLASS, name, source, library);
        vertex.addArc(Arc.Kind.Container,
                getVertex(Type.CLASS, className, source, library));
        vertex.addInheriting(superclass);
        for (String interfaceName : interfaces) {
            vertex.addInheriting(interfaceName);
        }
        return vertex;
    }

    public static JavaVertex declareInterface(String name,
            List<String> interfaces, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.INTERFACE, name, source, library);
        vertex.topVertex(source, library);
        for (String interfaceName : interfaces) {
            vertex.addInheriting(interfaceName);
        }
        return vertex;
    }

    public static JavaVertex declareInnerInterface(String name,
            String className, List<String> interfaces, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.INTERFACE, name, source, library);
        vertex.addArc(Arc.Kind.Container,
                getVertex(Type.CLASS, className, source, library));
        for (String interfaceName : interfaces) {
            vertex.addInheriting(interfaceName);
        }
        return vertex;
    }

    public static JavaVertex declareEnum(String name, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.ENUMERATION, name, source, library);
        vertex.topVertex(source, library);
        return vertex;
    }

    public static JavaVertex declareInnerEnum(String name, String className,
            String source, boolean library) {
        JavaVertex vertex = getVertex(Type.ENUMERATION, name, source, library);
        vertex.addArc(Arc.Kind.Container,
                getVertex(Type.CLASS, className, source, library));
        return vertex;
    }

    public JavaVertex declareField(String name, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.FIELD, this.name + '.' + name,
                source, library);
        vertex.addArc(Arc.Kind.Container, this);
        return vertex;
    }

    public JavaVertex declareMethod(String name, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.METHOD, this.name + '.' + name,
                source, library);
        vertex.addArc(Arc.Kind.Container, this);
        return vertex;
    }

    public JavaVertex declareConstructor(String name, String source, boolean library) {
        JavaVertex vertex = getVertex(Type.CONSTRUCTOR, this.name + '.' + name,
                source, library);
        vertex.addArc(Arc.Kind.Container, this);
        return vertex;
    }

    private static JavaVertex getVertex(Type type, String name, String source, boolean library) {
        JavaVertex vertex = getVertex(name);
        vertex.type = type;
        if (library) {
            vertex.visit(COHESION_LIBRARY);
        }
        return vertex;
    }

    static JavaVertex getVertex(String name) {
        JavaVertex vertex = GRAPH.get(name);
        if (vertex == null) {
            vertex = new JavaVertex(name);
            GRAPH.put(name, vertex);
        }
        return vertex;
    }

    private static void lookup(List<JavaVertex> entries, int howVisited) {
        for (JavaVertex entry : entries) {
            entry.pass(howVisited);
        }
    }
    
    private static void resolveUnknownTypesByReflection() {
        for(;;) {
            List<JavaVertex> unresolved = new ArrayList<JavaVertex>();
            for (JavaVertex vertex: GRAPH.values()) {
                if (vertex.type == Type.UNRESOLVED
                        && vertex.visit(COHESION_RESOLVED)
                        && vertex.name.indexOf('.') < 0 ) {
                    unresolved.add(vertex);
                }
            }
            if( unresolved.isEmpty() ) {
                return;
            }
            for(JavaVertex vertex: unresolved) {
                vertex.resolveTypeByReflection();
            }
        }
    }
    
    private void resolveTypeByReflection() {
        Class<?> cc;
        try {
            cc = Class.forName(name.replace('/', '.'));
        } catch (ClassNotFoundException e) {
            return;
        }
        if( cc.isEnum() ) {
            declareEnum(name, null, true);
        } else {
            Class<?> superclass = cc.getSuperclass();
            List<String> interfaces = new ArrayList<String>();
            for( Class<?> inf: cc.getInterfaces() ) {
                interfaces.add(inf.getName().replace('.', '/'));
            }
            if( superclass == null ) { 
                declareInterface(name, interfaces, null, true);
            } else {
                declareClass(name, superclass.getName().replace('.', '/'), interfaces, null, true);
            }
            for( Method m: cc.getDeclaredMethods() ) {
                StringBuffer mname = new StringBuffer(m.getName());
                Class<?>[] params = m.getParameterTypes();
                if( params.length == 0 ) {
                    mname.append("()");
                } else {
                    char separator = '(';
                    for( Class<?> param: params ) {
                        mname.append(separator);
                        separator = ',';
                        String pname = param.getName().replace('.', '/');
                        if( param.isArray() ) {
                            Parser.extractTypeNameFromDescriptor(mname, pname, 0);
                        } else {
                            if( !param.isPrimitive() ) {
                                getVertex(pname);
                            }
                            mname.append(pname);
                        }
                    }
                    mname.append(')');
                }
                JavaVertex methodVertex = declareMethod(mname.toString(), null, true);
                if( (m.getModifiers() & Modifier.ABSTRACT) != 0 ) {
                    addCalling(methodVertex.name);
                }
            }
        }
    }

    private static void bindOverridingMethods() {
        for (Iterator<JavaVertex> it = GRAPH.values().iterator(); it.hasNext();) {
            JavaVertex vertex = it.next();
            if (vertex.type == Type.METHOD) {
                JavaVertex host = vertex.container();
                String methodName = vertex.name
                        .substring(host.name.length() + 1);
                host.detectOverridings(vertex, methodName);
            }
        }
    }

    private void detectOverridings(JavaVertex method, String methodName) {
        for (Iterator<Arc> it = arcs.iterator(); it.hasNext();) {
            Arc arc = it.next();
            if (arc.kind == Arc.Kind.Inheriting) {
                JavaVertex superclass = arc.vertex;
                if (superclass.type != Type.UNRESOLVED) {
                    String name = superclass.name + '.' + methodName;
                    JavaVertex overriden = GRAPH.get(name);
                    if (overriden == null) {
                        superclass.detectOverridings(method, methodName);
                    } else {
                        overriden.addArc(Arc.Kind.Overridden, method);
                    }
                }
            }
        }
    }

    static class Arc {
        static enum Kind {
            Container, Overridden, Accessing, Inheriting, Invoking,
        }

        final Kind kind;
        final JavaVertex vertex;

        Arc(Kind kind, JavaVertex vertex) {
            this.kind = kind;
            this.vertex = vertex;
        }

        void pass(int howVisited) {
            if (howVisited == COHESION_PLAIN) {
                if (vertex.type == Type.CONSTRUCTOR) {
                    vertex.container().visit(COHESION_INSTANTIATED);
                }
                vertex.pass(COHESION_PLAIN);
            } else if (howVisited == COHESION_RESULTANT) {
                if (kind != Kind.Overridden
                        || vertex.container().cohesive(COHESION_INSTANTIATED)) {
                    vertex.pass(COHESION_RESULTANT);
                }
            }
        }
    }

    public static enum Type {
        LIBRARY,
        PACKAGE, 
        CLASS, 
        INTERFACE, 
        ENUMERATION, 
        UNRESOLVED, 
        FIELD, 
        METHOD, 
        CONSTRUCTOR,
    }

    public Type type;
    public final String name;
    List<Arc> arcs = new ArrayList<Arc>(); // The first arc is for
                                                   // membership

    private static final int COHESION_FORCED = 0x01; // First visiting for
                                                     // setting
                                                     // visitedViaConstructor
                                                     // in classes
    private static final int COHESION_PLAIN = 0x02; // First visiting for
                                                    // setting
                                                    // visitedViaConstructor in
                                                    // classes
    private static final int COHESION_INSTANTIATED = 0x04;
    private static final int COHESION_RESULTANT = 0x08; // Second visiting: does
                                                        // not go via OVERRIDING
                                                        // if father's
                                                        // visitedViaConstructor
                                                        // == false
    private static final int COHESION_STATIC  = 0x10; // First visiting for
    private static final int COHESION_METHOD  = 0x20; // First visiting for
    private static final int COHESION_RESOLVED= 0x40; // First visiting for
    private static final int COHESION_LIBRARY = 0x80; // First visiting for
                                                      // setting
                                                      // visitedViaConstructor
                                                      // in classes
    private int cohesion = 0;

    public void setCohesiveForced() {
        visit(COHESION_FORCED);
    }
    
    public boolean isStatic() {
        return cohesive(COHESION_STATIC);
    }
    
    public void setStatic() {
        visit(COHESION_STATIC);
    }
    
    public boolean isUnboundInvocation() {
        return type == Type.UNRESOLVED && cohesive(COHESION_METHOD);
    }

    public void addReference(String name) {
        if (name != null) {
            JavaVertex vertex = getVertex(name);
            addArc(Arc.Kind.Accessing, vertex);
        }
    }

    void addInheriting(String name) {
        if (name != null) {
            addInheriting(getVertex(name));
        }
    }

    public void addInheriting(JavaVertex vertex) {
        addArc(Arc.Kind.Inheriting, vertex);
    }

    public void addCalling(String name) {
        if (name != null) {
            JavaVertex vertex = getVertex(name);
            vertex.visit(COHESION_METHOD);
            addArc(Arc.Kind.Invoking, vertex);
        }
    }

    private void addArc(Arc.Kind kind, JavaVertex vertex) {
        arcs.add(new Arc(kind, vertex));
    }

    private JavaVertex(String name) {
        this.type = Type.UNRESOLVED;
        this.name = name;
    }

    JavaVertex container() {
        switch (type) {
        case LIBRARY:
        case PACKAGE:
        case UNRESOLVED:
            return null;
        default:
            return arcs.get(0).vertex;
        }
    }

    // private void reset() {
    // cohesion = 0;
    // }

    private boolean visit(int howVisited) {
        if (cohesive(howVisited)) {
            return false;
        }
        cohesion |= howVisited;
        return true;
    }

    boolean cohesive(int howVisited) {
        return (cohesion & howVisited) != 0;
    }

    private void pass(int howVisited) {
        if (visit(howVisited)) {
            for (Arc arc : arcs) {
                arc.pass(howVisited);
            }
        }
    }

    private String humanReadable() {
        StringBuffer synthetic = new StringBuffer();
        int pos, endpos;
        pos = 0;
        while( pos < name.length() ) {
            char c = name.charAt(pos++);
            synthetic.append(c);
            if( c == '$' ) {
                endpos = pos;
                while( endpos < name.length() && Character.isDigit(name.charAt(endpos)) ) {
                    endpos++;    
                }
                if( endpos > pos ) {
                    JavaVertex vertex = GRAPH.get(name.substring(0, endpos));
                    if( vertex != null && vertex.type == Type.CLASS ) {
                        String superclass = vertex.arcs.get(1).vertex.name;
                        if( superclass.equals("java/lang/Object") ) {
                            superclass = vertex.arcs.get(2).vertex.name;
                        }
                        synthetic.append(superclass.substring(superclass.lastIndexOf('/') + 1))
                            .append("_subclass_")
                            .append(name.substring(pos, endpos));
                    }
                    pos = endpos;
                }
            }
        }
        if( type == Type.CONSTRUCTOR ) {
            int initpos = synthetic.indexOf("<init>");
            if( initpos >= 0 ) {
                String containername = container().humanReadable();
                String constructorName;
                int internalPos = containername.lastIndexOf('$');
                if( internalPos >= 0 ) {
                    constructorName = containername.substring(internalPos + 1);
                } else {
                    constructorName = containername.substring(containername.lastIndexOf('/') + 1);
                }
                synthetic = synthetic.replace(initpos, initpos + 6, constructorName);
            }
        }
        return synthetic.toString();
    }
    
    @Override
    public String toString() {
        return String.format("%-12s%s", type.name(), humanReadable());
    }
}
