package classycle.test;

public class Test {
    
    static class A1 {
        void foo(B1 b, C1 c) {
        }
//        void foo(B1 b, C2 c) {
//        }
//        void foo(B2 b, C1 c) {
//        }
    }
    
    static class A2 extends A1 {
        void foo(B1 b, C1 c) {
            super.foo(b, c);
        }
    }
    
    static class B1 {
    }
    
    static class B2 extends B1 {
    }
    
    static class C1 {
    }
    
    static class C2 extends C1 {
    }
    
    public static void main(String[] args) {
        A2 a = new A2();
        B2 b = new B2();
        C2 c = new C2();
        
        a.foo(b,  c);
    }
}