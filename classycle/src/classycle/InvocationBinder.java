package classycle;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

class InvocationBinder {
    private static class MethodDescriptor {
        JavaVertex method;
        String name;
        JavaVertex thisType;
        List<JavaVertex> argTypes;
        List<Integer> argDimensions;
        
        MethodDescriptor(JavaVertex vertex) {
            method = vertex;
            int dotPoint = vertex.name.indexOf('.');
            int bracketPoint = vertex.name.indexOf('(', dotPoint);
            name = vertex.name.substring(dotPoint + 1, bracketPoint);
            thisType = JavaVertex.getVertex(vertex.name.substring(0, dotPoint));
            argTypes = new ArrayList<JavaVertex>();
            argDimensions = new ArrayList<Integer>();
            if( vertex.name.charAt(bracketPoint + 1) != ')' ) {
                for( int pos = bracketPoint + 1; pos < vertex.name.length(); pos++ ) {
                    int commaPoint = vertex.name.indexOf(',', pos);
                    if( commaPoint < 0 ) {
                        commaPoint = vertex.name.length() - 1;
                    }
                    String argType = vertex.name.substring(pos, commaPoint);
                    pos = commaPoint;
                    int arrayPoint = argType.indexOf('[');
                    if( arrayPoint < 0 ) {
                        argTypes.add(JavaVertex.getVertex(argType));
                        argDimensions.add(0);
                    } else {
                        argTypes.add(JavaVertex.getVertex(argType.substring(0, arrayPoint)));
                        argDimensions.add(argType.substring(arrayPoint).length());
                    }
                }
            }
        }
    }
    
    private static void addTargets(TreeMap<String, JavaVertex> digraph, JavaVertex type, String methodName, List<JavaVertex> targets) {
        String key = type.name + '.' + methodName + '(';
        for( JavaVertex vertex: digraph.tailMap(key).values() ) {
            if( !vertex.name.startsWith(key) ) break;
            if( vertex.type == JavaVertex.Type.METHOD ) {
                targets.add(vertex);
            }
        }
        for( JavaVertex.Arc arc: type.arcs ) {
            if( arc.kind == JavaVertex.Arc.Kind.Inheriting ) {
                addTargets(digraph, arc.vertex, methodName, targets);
            }
        }
    }
    
    static void bindMostSpecificMethods(TreeMap<String, JavaVertex> digraph) {
        JavaVertex d = JavaVertex.getVertex("double");
        JavaVertex f = JavaVertex.getVertex("float");
        JavaVertex l = JavaVertex.getVertex("long");
        JavaVertex i = JavaVertex.getVertex("int");
        JavaVertex c = JavaVertex.getVertex("char");
        JavaVertex s = JavaVertex.getVertex("short");
        JavaVertex b = JavaVertex.getVertex("byte");
        f.addInheriting(d);
        l.addInheriting(f);
        i.addInheriting(l);
        c.addInheriting(i);
        s.addInheriting(i);
        b.addInheriting(s);
        
        List<JavaVertex> unbound = new ArrayList<JavaVertex>(); 
        for( JavaVertex vertex: digraph.values() ) {
            if( vertex.isUnboundInvocation() ) {
                unbound.add(vertex);
            }
        }
        for( JavaVertex vertex: unbound ) {
            bindMostSpecificMethod(digraph, vertex);
        }
    }
    
    private static void bindMostSpecificMethod(TreeMap<String, JavaVertex> digraph, JavaVertex vertex) {
        MethodDescriptor invocation = new MethodDescriptor(vertex);
        List<JavaVertex> targets = new ArrayList<JavaVertex>();
        addTargets(digraph, invocation.thisType, invocation.name, targets);
        JavaVertex target = mostSpecificMethod(invocation, targets);
        if( target != null ) {
            vertex.addCalling(target.name);
        }
    }
    
    private static int typeCoercionDistance(JavaVertex from, JavaVertex to) {
        if( from == to ) {
            return 0;
        }
        int result = Integer.MAX_VALUE;
        for( JavaVertex.Arc arc: from.arcs ) {
            if( arc.kind == JavaVertex.Arc.Kind.Inheriting ) {
                int distance = typeCoercionDistance(arc.vertex, to);
                if( distance < result ) {
                    result = distance;
                }
            }
        }
        return result == Integer.MAX_VALUE ? Integer.MAX_VALUE : result + 1;
    }
    
    private static int signatureDistance(MethodDescriptor invocation, MethodDescriptor target) {
        if( invocation.argTypes.size() != target.argTypes.size() ) {
            return Integer.MAX_VALUE;
        }
        int argDistance;
        if( target.method.isStatic() ) {
            argDistance = 0;
        } else {
            argDistance = typeCoercionDistance(invocation.thisType, target.thisType);
        }
        if( argDistance == Integer.MAX_VALUE ) {
            return Integer.MAX_VALUE;
        }
        for( int i = 0; i < invocation.argTypes.size(); i++ ) {
            if( invocation.argDimensions.get(i).intValue() != target.argDimensions.get(i).intValue() ) {
                return Integer.MAX_VALUE;
            }
            int distance = typeCoercionDistance(invocation.argTypes.get(i), target.argTypes.get(i));
            if( distance == Integer.MAX_VALUE ) {
                return Integer.MAX_VALUE;
            }
            argDistance += distance;
        }
        return argDistance;
    }
    
    private static JavaVertex mostSpecificMethod(MethodDescriptor invocation, List<JavaVertex> targets) {
        JavaVertex bestTarget = null;
        int minDistance = Integer.MAX_VALUE;
        for( JavaVertex target: targets ) {
            int distance = signatureDistance(invocation, new MethodDescriptor(target));
            if( distance < minDistance ) {
                minDistance = distance;
                bestTarget = target;
            }
        }
        return bestTarget;
    }
}
